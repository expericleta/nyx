#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

// OLED display TWI address
#define OLED_ADDR   0x3C


int pin = 13;
volatile int state = LOW;
unsigned long diferencia = 0;
unsigned long anterior = 0;

float radio = 0.3;
float velocidad = 0;
int vmax=100;



Adafruit_SSD1306 display(-1);

void setup()
{ Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();

 
  pinMode(pin, OUTPUT);
//  digitalWrite(2, HIGH);
  pinMode(2, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(2), toggle, RISING); // Attaching the ISR to INT0

  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(10,38);
  display.print("BICI NYX");
  display.setTextSize(1);
  display.setCursor(27,24);
  display.print("por Pedaludico");
  display.display();
  delay(500);
}

void loop()
{
  unsigned long currentMillis = millis();
  if(state==HIGH&&((currentMillis-anterior)>68))
  {
    diferencia=currentMillis-anterior;
    velocidad=2*3.14*radio/diferencia*3600;

    
    Serial.println(velocidad);
//    imprimir(velocidad);
    anterior=currentMillis;
    state=LOW;
  }
}

// Interrupt Service Routine
void toggle()
{
  state=HIGH;

}

//void imprimir(float vel)
//{    
//    display.clearDisplay();
//    display.setTextSize(2);
//    display.setTextColor(WHITE);
//    display.setCursor(27,38);
//    display.print(vel);
//    display.display();
//    
//}
